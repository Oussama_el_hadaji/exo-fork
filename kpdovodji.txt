On va commencer par voir comment supprimer un dépôt sur GitLab. Si vous
ne voulez pas supprimer votre premier et seul dépôt, créez-en un
nouveau.

Depuis la page principale du dépôt à supprimer, aller dans
« Settings > General ». Puis, dans la section « Advanced », tout en
bas → « Delete project ».

Remarque : c'est compliqué de supprimer un projet. GitLab cherche à
compliquer la tâche puisque alors on perd tout le travail effectué et
l'historique du projet. À ne faire que si le projet ne contient pas de
travail (comme ici), sinon on risque de le regretter...
Voyons maintenant comment l'on peut « proposer des commits » sur un
projet qui ne nous appartient pas.